const { config } = require('../wdio.shared123.conf');

// ============
// Specs
// ============
config.cucumberOpts.require = ['test/Steps/*.steps.js'];

// ============
// Capabilities
// ============
// For all capabilities please check
// http://appium.io/docs/en/writing-running-appium/caps/#general-capabilities
config.capabilities = [
    {
        // deviceName: 'Samsung Galaxy S9*',
        // automationName: 'UiAutomator2',
        // The reference to the app
        // testobject_app_id: '5',
        // The api key that has a reference to the app-project in the TO cloud
        testobject_api_key: "A049C2C096A84E07B6122289E8BB1324",
        // The name of the test for in the cloud
        // testobject_test_name: 'wdio-demo-app-test',
        // Some default settings
        // You can find more info in the TO Appium Basic Setup section
        platformName: 'Android',
        platformVersion: '10',
        // idleTimeout: 180,
        maxInstances: 1,
        testobject_cache_device: false,
        noReset: false,
        orientation: 'PORTRAIT',
        newCommandTimeout: 0,
        // phoneOnly: true,
        // tabletOnly: false,
        // app: 'storage:filename=Android.SauceLabs.Mobile.Sample.app.2.4.0.apk',
        'appium:newCommandTimeout': 0,
        
        "platformName": "Android",
        // "automationName": "UiAutomator2",
        // "uiid": "emulator-5554",
        "appPackage": "com.enouvo.smartos",
        "appActivity": ".MainActivity"
    },
];

// =========================
// Sauce RDC specific config
// =========================
// The new version of WebdriverIO will:
// - automatically update the job status in the RDC cloud
// - automatically default to the US RDC cloud
config.services = ['sauce'];
// If you need to connect to the US RDC cloud comment the below line of code
// config.region = 'eu';
// and uncomment the below line of code
config.region = 'us';

// This port was defined in the `wdio.shared.conf.js`
delete config.port;

exports.config = config;
