const nodemailer = require('nodemailer');
var fs = require('fs');
const hbs = require('nodemailer-express-handlebars')
const log = console.log;
const path = require('path');
const data = require('../../allure-report/allure-report/widgets/summary.json')
const mailList = [
    'hung.nguyen@enouvo.com',
    'trinh.nguyen@enouvo.com'
]

const today = new Date()
const month = today.toLocaleString('default', { month: 'long' })
const date = today.toLocaleString('default', { day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit', timeZoneName: 'long'})
const total = data.statistic.total;
const passed = data.statistic.passed;
const failed = data.statistic.failed;
let result = "Failed";
if( total === passed) {
    result = "Passed";
}
const workingDir = process.cwd();


const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: process.env.SEND_EMAIL_ACCOUNT,
        pass: process.env.SEND_PASSWORD_ACCOUNT
    }
});

console.log("Check email path",path.resolve(workingDir, "e2e/config/views/"));


// transporter.use('compile', hbs({
//     viewEngine: 'express-handlebars',
//     viewPath: './report/views/'
// }));

const handlebarOptions = {
    viewEngine: {
      extName: ".handlebars",
      partialsDir: path.resolve(workingDir, "e2e/config/views/"),
      defaultLayout: false,
    },
    viewPath: path.resolve(workingDir, "e2e/config/views/"),
    extName: ".handlebars",
  };

transporter.use(
    "compile",
    hbs(handlebarOptions)
  );

let mailOptions  = {
    from : process.env.SEND_EMAIL_ACCOUNT,
    to: mailList,
    subject: 'Automation Tests - KindiCare - ' + result,
    text: 'Report via email',
    template: 'index',
    context: {
        name: 'Enouvo',
        linkReport: 'https://kindicare-automation-ui-report.web.app',
        title: "Automation UI report",
        content: 'This is hourly report for KindiCare',
        month: month,
        total: total,
        passed: passed,
        failed: failed,
        date: date,
        enouvoSite: 'https://enouvo.com',
        enouvoIcon: 'https://i.ibb.co/yVnfHM0/LOGO-EIS-11.png'

    }, 
    
}

transporter.sendMail(mailOptions, function (err,data){
    if(err){
        console.log('Error occur', err);
    } else {
        console.log('Mail sent');
    }
})


