class AndroidInfo {
    static deviceName() {
        return 'Bi'; // pass the udid or devicename
    }

    static platFormVersion() {
        return '10'; // pass the platform version
    }

    static appName() {
        return 'kindi-staging.apk'; // pass the apk name
    }

    static appPackage() {
        return 'com.enouvo.kindicare'; // pass the apk name
    }
}

module.exports = AndroidInfo;
