const reporter = require('cucumber-html-reporter');

const options = {
  theme: 'bootstrap',
  jsonFile: 'test-report/cucumber-report/',
  output: 'test-report/cucumber-report/html/cucumber_report.html',
  reportSuiteAsScenarios: true,
  scenarioTimestamp: true,
  launchReport: true,
  ignoreBadJsonFile: true,
  metadata: {
    "App Version": "1.0.0",
    "Test Environment": "STAGING",
    "Platform": "Android123",
    "Parallel": "Scenarios",
    "Executed": "Remote"
  }
};

reporter.generate(options);
