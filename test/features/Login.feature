Feature: Login

   I login and create new my timesheet

   Scenario: Login empty data 
   # Given I open device
   Then I wait on element "flashScreen" for 20000ms to be displayed by "myAppiumAppr"
   Then I wait on element "nextAtTutorialPage" for 20000ms to be displayed by "myAppiumAppr"
   When I click on the element "nextAtTutorialPage" by "myAppiumAppr"
   Then I wait on element "nextAtTutorialPage" for 20000ms to be displayed by "myAppiumAppr"
   When I click on the element "nextAtTutorialPage" by "myAppiumAppr"
   Then I wait on element "nextAtTutorialPage" for 20000ms to be displayed by "myAppiumAppr"
   When I click on the element "nextAtTutorialPage" by "myAppiumAppr"
   Then I wait on element "getStartedAtTutorialPage" for 20000ms to be displayed by "myAppiumAppr"
   When I click on the element "getStartedAtTutorialPage" by "myAppiumAppr"
   Then I wait on element "acceptUseLocation" for 20000ms to be displayed by "myAppiumAppr"
   When I click on the element "acceptUseLocation" by "myAppiumAppr"
   Then I wait on element "searchInput" for 20000ms to be displayed by "myAppiumAppr"
   When I click on the element "searchInput" by "myAppiumAppr"
   And I pause for 5000ms by "myChromeBrowser"

   Given I open the site "/login" by "myChromeBrowser" 
   Then I expect that the title is "KindiCare" by "myChromeBrowser"
   When I set "admin@kindicare.com" to the inputfield "emailInputWeb" by "myChromeBrowser"
   And I set "enouvo123" to the inputfield "passwordInputWeb" by "myChromeBrowser"
   And I click on the element "loginInputWeb" by "myChromeBrowser"
   Then I wait on element "dashboardUserInfo" for 20000ms to be displayed by "myChromeBrowser"
   When I click on the element "centresWeb" by "myChromeBrowser"
   And I click on the element "createCentresWeb" by "myChromeBrowser"
   And I set "Hung Nguyen" random to the inputfield "inputNameCentresCreateWeb" by "myChromeBrowser"
   And I set "HungBusiness" random to the inputfield "inputBusinessCentresCreateWeb" by "myChromeBrowser"
   And I set "hung.nguyen+enquiry" random email to the inputfield "inputEnquiryEmailCentresCreateWeb" by "myChromeBrowser"
   And I set "hung.nguyen+admin" random email to the inputfield "adminEmailCentresCreateWeb" by "myChromeBrowser"
   And I set "hung.nguyen+centres" random email to the inputfield "emailCentresCreateWeb" by "myChromeBrowser"
   And I set "Viet Nam" to the inputfield "countryCentresCreateWeb" by "myChromeBrowser"
   And I set "Da Nang" to the inputfield "cityCentresCreateWeb" by "myChromeBrowser"
   And I set "550000" to the inputfield "postCodeCentresCreateWeb" by "myChromeBrowser"
   And I set "15 Ta My Duat" to the inputfield "addressCentresCreateWeb" by "myChromeBrowser"
   And I set "15 Tạ Mỹ Duật, An Hải, An Hải Bắc, Sơn Trà, Da Nang, Vietnam" to the inputfield "locationCentresCreatWeb" by "myChromeBrowser"
   And I click on the element "contentTypeCentresCreateWeb" by "myChromeBrowser"
   And I press "Enter" by "myChromeBrowser"
   And I click on the element "stateCentresCreateWeb" by "myChromeBrowser"
   And I press "Enter" by "myChromeBrowser"
   And I pause for 5000ms by "myChromeBrowser"
   And I click on the element "createButtonCentresCreateWeb" by "myChromeBrowser"
   And I pause for 2000ms by "myChromeBrowser"
   Then I wait on element "successMessage" for 20000ms to be displayed by "myChromeBrowser"
   # And I enter "" data to Email input
   # And I enter "" data to Password input
   # And I click on Sign in button
   # And I dont see menu button
   # And I close device

   # Scenario: Login correct data and create new my timesheet
   # # Given I open device
   # When I see the login page
   # And I enter "nguyen.pham@enouvo.com" data to Email input
   # And I enter "password" data to Password input
   # And I click on Sign in button
   # And I click on menu button
   # And I click on mytimesheet button 
   # And I click on datetimePicker
   # And I click on "01" date and "October" month
   # And I click on OK date
   # And I click on New entry 
   # And I click on Start time
   # And I select AM 
   # And I select one hour
   # And I click on OK time button
   # And I click on End time
   # And I select PM
   # And I select five hour
   # And I click on OK time button
   # And I select leave type
   # And I select first leave type
   # And I select time allow
   # And I select first time allow
   # And I select LAHA
   # And I select first LAHA
   # And I click on approve button
   # And I click on dropdown entry button
   # And I click on delete button
   # And I click on delete confirm button
   # And I click on Ok confirm delete button
   # And I click back to home page
   # # And I close device

   