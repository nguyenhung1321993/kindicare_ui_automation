/**
 * Check if the given element exists in the DOM one or more times
 * @param  {String}  selector  Element selector
 * @param  {Boolean} falseCase Check if the element (does not) exists
 * @param  {Number}  exactly   Check if the element exists exactly this number
 *                             of times
 */
export default (selector, falseCase, exactly, platform) => {

    let platform2;

    switch (platform) {
        case Object.keys({myChromeBrowser})[0]:
            platform2 = myChromeBrowser;
            break;
        case Object.keys({myAppiumAppr})[0]:
            platform2 = myAppiumAppr;
            break;
        default:
            platform2 = myChromeBrowser;
            break;
    }

    console.log('platform', platform )
    console.log('selector123', selector )
    console.log('selector', platform2 )
    /**
     * The number of elements found in the DOM
     * @type {Int}
     */
    const nrOfElements = platform2.$$(selector);

    if (falseCase === true) {
        expect(nrOfElements).toHaveLength(
            0,
            `Element with selector "${selector}" should not exist on the page`
        );
    } else if (exactly) {
        expect(nrOfElements).toHaveLength(
            exactly,
            `Element with selector "${selector}" should exist exactly `
            + `${exactly} time(s)`
        );
    } else {
        expect(nrOfElements.length).toBeGreaterThanOrEqual(
            1,
            `Element with selector "${selector}" should exist on the page`
        );
    }
};
