/**
 * Handle a modal
 * @param  {String}   action    Action to perform on the modal (accept, dismiss)
 * @param  {String}   modalType Type of modal (alertbox, confirmbox, prompt)
 */
export default (action, modalType, platform) => {


    let platform2;

    switch (platform) {
        case Object.keys({myChromeBrowser})[0]:
            platform2 = myChromeBrowser;
            break;
        case Object.keys({myAppiumAppr})[0]:
            platform2 = myAppiumAppr;
            break;
        default:
            platform2 = myChromeBrowser;
            break;
    }

    /**
     * The command to perform on the browser object
     * @type {String}
     */
    let command = `${action.slice(0, 1).toLowerCase()}${action.slice(1)}Alert`;

    /**
     * Alert boxes can't be dismissed, this causes Chrome to crash during tests
     */
    if (modalType === 'alertbox') {
        command = 'acceptAlert';
    }

    platform2[command]();
};
