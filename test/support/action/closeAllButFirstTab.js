/**
 * Close all but the first tab
 * @param  {String}   obsolete Type of object to close (window or tab)
 */
/* eslint-disable no-unused-vars */
export default (obsolete, platform) => {

    let platform2;

    switch (platform) {
        case Object.keys({myChromeBrowser})[0]:
            platform2 = myChromeBrowser;
            break;
        case Object.keys({myAppiumAppr})[0]:
            platform2 = myAppiumAppr;
            break;
        default:
            platform2 = myChromeBrowser;
            break;
    }

/* eslint-enable no-unused-vars */
    /**
     * Get all the window handles
     * @type {Object}
     */
    const windowHandles = platform2.getWindowHandles();

    // Close all tabs but the first one
    windowHandles.reverse();
    windowHandles.forEach((handle, index) => {
        platform2.switchToWindow(handle);
        if (index < windowHandles.length - 1) {
            platform2.closeWindow();
        }
    });
};
