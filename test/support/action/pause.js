/**
 * Pause execution for a given number of milliseconds
 * @param  {String}   ms   Number of milliseconds to pause
 */
export default (ms, platform) => {

    let platform2;

  switch (platform) {
      case myChromeBrowser.constructor.name:
          platform2 = myChromeBrowser;
          break;
      case myAppiumAppr.constructor.name:
          platform2 = myAppiumAppr;
      default:
          platform2 = myChromeBrowser;
          break;
  }

    /**
     * Number of milliseconds
     * @type {Int}
     */
    const intMs = parseInt(ms, 10);

    platform2.pause(intMs);
};
