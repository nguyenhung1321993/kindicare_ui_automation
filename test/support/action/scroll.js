import { loginPage } from '../../pageObjects/exportPage'
import { homePage } from '../../pageObjects/exportPage'

/**
 * Scroll the page to the given element
 * @param  {String}   selector Element selector
 */
export default (selector, platform) => {

    let platform2;

    switch (platform) {
        case Object.keys({myChromeBrowser})[0]:
            platform2 = myChromeBrowser;
            break;
        case Object.keys({myAppiumAppr})[0]:
            platform2 = myAppiumAppr;
            break;
        default:
            platform2 = myChromeBrowser;
            break;
    }
    const selector2 = (((loginPage)[selector])||((homePage)[selector]));
    platform2.$(selector2).scrollIntoView();
};
