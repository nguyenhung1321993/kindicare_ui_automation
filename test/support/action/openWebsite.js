/**
 * Open the given URL
 * @param  {String}   type Type of navigation (getUrl or site)
 * @param  {String}   page The URL to navigate to
 */
        export default (type, page, platform) => {
    /**
     * The URL to navigate to
     * @type {String}
     */
    let platform2;

    switch (platform) {
        case Object.keys({myChromeBrowser})[0]:
            platform2 = myChromeBrowser;
            break;
        case Object.keys({myAppiumAppr})[0]:
            platform2 = myAppiumAppr;
            break;
        default:
            platform2 = myChromeBrowser;
            break;
    }

    // browser.windowHandleFullscreen();
    platform2.maximizeWindow();
    const url = (type === 'url') ? page : platform2.options.baseUrl + page;
    platform2.url(url);
    
    // browser.setWindowSize(1920,1080);
};
