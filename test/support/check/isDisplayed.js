import checkIfElementExists from '../lib/checkIfElementExists';
import { loginPage } from '../../pageObjects/exportPage'
import { homePage } from '../../pageObjects/exportPage'

/**
 * Check if the given element is (not) visible
 * @param  {String}   selector   Element selector
 * @param  {String}   falseCase Check for a visible or a hidden element
 */
export default (selector, falseCase, platform) => {

    let platform2;

    switch (platform) {
        case Object.keys({myChromeBrowser})[0]:
            platform2 = myChromeBrowser;
            break;
        case Object.keys({myAppiumAppr})[0]:
            platform2 = myAppiumAppr;
            break;
        default:
            platform2 = myChromeBrowser;
            break;
    }

    const selector2 = (((loginPage)[selector])||((homePage)[selector]));
    /**
     * Visible state of the give element
     * @type {String}
     */
    const isDisplayed = platform2.$(selector2).isDisplayed();

    if (falseCase) {
        expect(isDisplayed).not.toEqual(
            true,
            `Expected element "${selector2}" not to be displayed`
        );
    } else {
        expect(isDisplayed).toEqual(
            true,
            `Expected element "${selector2}" to be displayed`
        );
    }
};
