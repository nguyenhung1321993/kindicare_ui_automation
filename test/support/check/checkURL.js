/**
 * Check the URL of the given browser window
 * @param  {String}   falseCase   Whether to check if the URL matches the
 *                                expected value or not
 * @param  {String}   expectedUrl The expected URL to check against
 */
export default (falseCase, expectedUrl, platform) => {

    let platform2;

    switch (platform) {
        case Object.keys({myChromeBrowser})[0]:
            platform2 = myChromeBrowser;
            break;
        case Object.keys({myAppiumAppr})[0]:
            platform2 = myAppiumAppr;
            break;
        default:
            platform2 = myChromeBrowser;
            break;
    }

    /**
     * The current browser window's URL
     * @type {String}
     */
    const currentUrl = platform2.getUrl();

    if (falseCase) {
        expect(currentUrl)
            .not.toEqual(expectedUrl, `expected url not to be "${currentUrl}"`);
    } else {
        expect(currentUrl).toEqual(
            expectedUrl,
            `expected url to be "${expectedUrl}" but found `
            + `"${currentUrl}"`
        );
    }
};
