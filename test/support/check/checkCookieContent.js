/**
 * Check the content of a cookie against a given value
 * @param  {String}   name          The name of the cookie
 * @param  {String}   falseCase     Whether or not to check if the value matches
 *                                  or not
 * @param  {String}   expectedValue The value to check against
 */
export default (name, falseCase, expectedValue, platform) => {

    let platform2;

    switch (platform) {
        case Object.keys({myChromeBrowser})[0]:
            platform2 = myChromeBrowser;
            break;
        case Object.keys({myAppiumAppr})[0]:
            platform2 = myAppiumAppr;
            break;
        default:
            platform2 = myChromeBrowser;
            break;
    }

    /**
     * The cookie retrieved from the browser object
     * @type {Object}
     */
    const cookie = platform2.getCookies(name)[0];
    expect(cookie.name).toBe(
        name,
        `no cookie found with the name "${name}"`
    );

    if (falseCase) {
        expect(cookie.value).not.toBe(
            expectedValue,
            `expected cookie "${name}" not to have value "${expectedValue}"`
        );
    } else {
        expect(cookie.value).toBe(
            expectedValue,
            `expected cookie "${name}" to have value "${expectedValue}"`
            + ` but got "${cookie.value}"`
        );
    }
};
