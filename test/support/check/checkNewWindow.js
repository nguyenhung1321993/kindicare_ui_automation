/**
 * Check if a new window or tab is opened
 * @param  {String}   obsolete  The type of opened object (window or tab)
 * @param  {String}   falseCase Whether to check if a new window/tab was opened
 *                              or not
 */
export default (obsolete, falseCase, platform) => {

    let platform2;

    switch (platform) {
        case Object.keys({myChromeBrowser})[0]:
            platform2 = myChromeBrowser;
            break;
        case Object.keys({myAppiumAppr})[0]:
            platform2 = myAppiumAppr;
            break;
        default:
            platform2 = myChromeBrowser;
            break;
    }

    /**
     * The handles of all open windows/tabs
     * @type {Object}
     */
    const windowHandles = platform2.getWindowHandles();

    if (falseCase) {
        expect(windowHandles)
            .toHaveLength(1, 'A new window should not have been opened');
    } else {
        expect(windowHandles)
            .not.toHaveLength(1, 'A new window has been opened');
    }
};
